<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function(){
    Route::post('/signup', [AuthController::class, 'register'])->name('register');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::get('/roles', [RoleController::class, 'index'])->name('role.index');
    Route::group(['middleware' => ['auth:sanctum']], function(){

        //logout
        // Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
        //roleController
        Route::post('/role/create', [RoleController::class, 'store'])->name('role.store');
        Route::delete('/role/{id}', [RoleController::class, 'delete'])->name('role.delete');

        Route::delete('/user/{id}', [AdminController::class, 'removeUser'])->name('user.delete');
        Route::get('/user/{id}', [UserController::class, 'userDetail'])->name('user.detail');
        
    });
});
