<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['role' => 'HR', 'is_admin'=>1],
            ['role' => 'Scrum Masters', 'is_admin'=>1],
            ['role' => 'Product Owners', 'is_admin'=>1],
            ['role' => 'Developers', 'is_admin'=>0],
        ];
        Role::insert($roles);
    }
}
