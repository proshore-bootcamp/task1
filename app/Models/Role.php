<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Role extends Model
{
    use HasFactory, HasApiTokens, Notifiable;

    protected $fillable = ['role', 'is_admin'];

    public function users(){
        return $this->hasMany(User::class);
    }
    protected $hidden = ['created_at', 'updated_at', 'id'];
}
