<?php

namespace App\Repositories\Implementation;

use App\Models\User;
use App\Repositories\Interfaces\IAuthRepository;
use Illuminate\Support\Facades\Hash;

class AuthRepository implements IAuthRepository{
    protected $user;

    public function __construct(User $user) {
        $this->user = $user;
    }

    public function register($data): array{
        $user = new $this->user;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->role_id = $data['role_id'];

        $user->save();
        $this->user = $user;
        $token = $user->createToken('auth_token', [$user->role->is_admin ? 'admin' : 'user']);
        return [
            'user' => $user->toJson(),
            'access_token' => $token->plainTextToken
        ];
    }

    public function login($data): array{
        $this->user = User::where('email', $data['email'])->firstOrFail();
        $token = $this->user->createToken('auth_token',[$this->user->role->is_admin ? 'admin' : 'user']);
        return [
            'data' => $this->user->toJson(),
            'access_token' => $token->plainTextToken
        ];
    }

    // public function logout():bool{
    //     $user = Auth::user();
    //     return $user->currentAccessToken()->delete();;
    // }
}