<?php

namespace App\Repositories\Implementation;

use App\Repositories\Interfaces\IEloquentRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements IEloquentRepository{

    protected $model;

    public function __construct(Model $model){
        $this->model = $model;
    }

    public function create(array $attributes): Model{
        return $this->model->create($attributes);
    }

    public function find($id): ?Model{
        return $this->model->find($id);
    }

    public function getAll(): Collection{
        return $this->model->all();
    }

    public function delete($id): ?bool{
        return $this->model->destroy($id);
    }
}