<?php

namespace App\Repositories\Implementation;

use App\Models\Role;
use App\Repositories\Interfaces\IRoleRepository;

class RoleRepository extends BaseRepository implements IRoleRepository{

    public function __construct(Role $model) {
        parent::__construct($model);
    }

    // public function update
}