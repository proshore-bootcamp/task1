<?php

namespace App\Repositories\Implementation;

use App\Repositories\Interfaces\IUserRepository;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserRepository extends BaseRepository implements IUserRepository{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function isAdmin():bool{
        return Auth::user()->role->is_admin;
    }

    public function deleteToken($user):bool{
        return $user->tokens()->delete();
    }
}