<?php

namespace App\Repositories\Interfaces;


interface IAuthRepository{
    function register($data):array;
    function login($data):array;
    // function logout():bool;
}