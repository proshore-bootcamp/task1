<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface IEloquentRepository
{

    public function create(array $attributes): Model;

    public function find($id): ?Model;
    public function getAll(): Collection;
}