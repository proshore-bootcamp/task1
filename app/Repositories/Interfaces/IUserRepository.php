<?php

namespace App\Repositories\Interfaces;

interface IUserRepository{
    function isAdmin():bool;
    function deleteToken($user);
}