<?php

namespace App\Http\Library;

use Illuminate\Http\JsonResponse;

trait ApiHelpers {
    protected function isAdmin($user): bool
    {
        if (!empty($user)) {
            return $user->tokenCan('admin');
        }
        return false;
    }
    protected function isUser($user): bool
    {
        if (!empty($user)) {
            return $user->tokenCan('user');
        }

        return false;
    }
    
}