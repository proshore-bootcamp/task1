<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelpers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\Implementation\UserService;

class UserController extends BaseController
{
    use ApiHelpers;
    protected $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    public function userDetail(Request $request):JsonResponse{
        $id = $request->id;
        if($this->isAdmin($request->user()) || $this->isUser($request->user())){
            if($this->isAdmin($request->user())){
                $user =  $this->userService->detail($id);
                return $this->successResponse($user);
            }
            if($request->user()->id == $id){
                $user = $this->userService->detail($id);
                return $this->successResponse($user);
            }
        }
        return $this->errorResponse(401, 'Unauthorized');
    }
}
