<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\Implementation\AuthService;
use Exception;
use Illuminate\Http\JsonResponse;

class AuthController extends BaseController
{
    protected $authService;

    public function __construct(AuthService $authService){
        $this->authService = $authService;
    }

    public function register(Request $request):Object{
        $data = $request->only([
            'name','email','password', 'password_confirmation','role_id'
        ]);
        try{
            $result = ['status' => 201];
            $result['data'] = $this->authService->registerUser($data);
        }catch(Exception $e){
            $result = [
                "status" => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function login(Request $request):JsonResponse{
        $data = $request->only(['email', 'password']);
        $result = $this->authService->loginUser($data);
        if($result == []){
            return $this->errorResponse(401, 'Invalid Credentials');
        }
        return $this->successResponse($result, 'Login Successfull');
    }

    // public function logout():Object{
    //     $response = $this->authService->logoutUser();
    //     if($response){
    //         $result = [
    //             'status'=> 200,
    //             'message'=>"User logout successfully.",
    //         ];
    //     }else{
    //         $result = [
    //             'status'=> 204,
    //             'message' => 'Logout failed',
    //         ];
    //     }
    //     return response()->json($result, $result['status']);
    // }
}
