<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\Implementation\RoleService;
use Exception;
use Illuminate\Support\Facades\Auth;

class RoleController extends BaseController
{
    use ApiHelpers;
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index():Object
    {
        $roles = $this->roleService->getAllRoles();
        return $this->successResponse($roles);
    }

    public function store(Request $request): Object 
    {
        if($this->isAdmin($request->user())){
            $data = $request->only(['role', 'is_admin']);
            try{
                $result = ['status' => 201];
                $result['data'] = $this->roleService->storeData($data);
            }catch(Exception $e){
                $result = [
                    'status' => 422,
                    'errors' => json_decode($e->getMessage())
                ];
            }
            return response()->json($result, $result['status']);
        }
        return $this->errorResponse(403, 'Forbidden');
    }

    public function delete(Request $request): Object{
        if($this->isAdmin($request->user())){
            $status = $this->roleService->deleteRole($request->id);
            if($status){
                return $this->successResponse([], 'Role deleted');
            }
            return $this->errorResponse(404, 'Role not found');
        }
        return $this->errorResponse(403, 'Forbidden');
    }
}
