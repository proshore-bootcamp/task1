<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelpers;
use App\Services\Implementation\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    use ApiHelpers;
    protected $userService;
    public function __construct(UserService $userService){
        $this->userService = $userService;
    }
    public function removeUser(Request $request, $id): JsonResponse{

        if($this->isAdmin($request->user())){
            $res = $this->userService->remove($id);
            return $this->onSuccess([], 'User Removed.');
        }
        return $this->errorResponse(403, 'Unauthorized');
    }
}
