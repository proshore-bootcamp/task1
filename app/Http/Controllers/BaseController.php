<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{
    protected function successResponse($data, $message = '', $code = 200): JsonResponse
    {
        return response()->json([
            'status' => $code,
            'message' => $message,
            'data' => $data,
        ], $code);
    }
    protected function errorResponse($code, $message = '', $errors=[]): JsonResponse
    {
        $response = [
            'status' => $code,
            'message' => $message,
        ];
        if($errors != []){
            $response['errors'] = $errors;
        };
        return response()->json($response, $code);
    }
}
