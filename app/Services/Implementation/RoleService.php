<?php

namespace App\Services\Implementation;

use App\Services\Interfaces\IRoleService;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Implementation\RoleRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;
use ModelNotFoundException;
use Exception;
use Illuminate\Support\Facades\Auth;

class RoleService implements IRoleService{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getAllRoles(): Collection
    {
        return $this->roleRepository->getAll();
    }

    public function storeData($data): Model
    {
        $validator = Validator::make($data, [
            'role'=> 'required|max:255',
        ]);
        if($validator->fails()){
            throw new InvalidArgumentException($validator->errors());
        }
        $result = $this->roleRepository->create($data); 
        return $result;
    }

    public function deleteRole($id): ?bool{
        return $this->roleRepository->delete($id);
    }

}