<?php

namespace App\Services\Implementation;
use App\Services\Interfaces\IAuthService;
use App\Repositories\Implementation\AuthRepository;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthService implements IAuthService{

    protected $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function registerUser($data):array{
        $validator = Validator::make($data,[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'role_id' => 'required'
        ]);

        if($validator->fails()){
            throw new InvalidArgumentException($validator->errors());
        }
        $result = $this->authRepository->register($data);
        return $result;
    }

    public function loginUser($data):array{
        $validator = Validator::make($data, [
            'email' => 'required|string|email|max:255',
            'password'=>'required|string',
        ]);
        if(!Auth::attempt(['email' => $data->email, 'password' => $data->password])){
            return [];
        }
        $result = $this->authRepository->login($data);
        return $result;
    }

    // public function logoutUser():bool{
    //     Session::flush();
    //     return $this->authRepository->logout();
    // }


}