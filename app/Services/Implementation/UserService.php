<?php

namespace App\Services\Implementation;

use App\Http\Library\ApiHelpers;
use App\Repositories\Implementation\UserRepository;
use App\Services\Interfaces\IUserService;
use Illuminate\Database\Eloquent\Model;

class UserService implements IUserService{

    use ApiHelpers;
    protected $userRepository;

    public function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
    }

    public function remove($id):bool{
        if($this->userRepository->isAdmin()){
            $user = $this->userRepository->find($id);
            
            $this->userRepository->deleteToken($user);
            $this->userRepository->delete($id);
            
            return 1;
        }else{
            return 0;
        }
    }

    public function detail($id):Model{
        $user = $this->userRepository->find($id);
        return $user;
    }
}