<?php

namespace App\Services\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface IRoleService{
    public function storeData($data): Model;
    public function getAllRoles():Collection;
    public function deleteRole($id):?bool;
}