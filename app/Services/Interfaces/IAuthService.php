<?php

namespace App\Services\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface IAuthService{
    function registerUser($data): array;
    // public function loginUser($data);
    // function logoutUser():bool;
}