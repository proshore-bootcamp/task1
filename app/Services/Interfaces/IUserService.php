<?php 

namespace App\Services\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface IUserService{
    function remove($id):bool;
    function detail($id):Model;
}